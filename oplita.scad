use <corpo_oplita.scad>
$fn=40;
translate([0,-8.2,2]){
cylinder(r=1.4,h=35); //lancia
translate([0,0,35]) cylinder(r1=1.4,r2=0,h=3); //punta lancia
}
//cylinder(r1=12,r2=4,h=18); //corpo
/********CORPO********/
scale([0.054,0.054,0.054])rotate_extrude()translate([150,0,0])mirror([1,-1,0])difference(){
    union(){
        translate([0,-150,0])square([400,150]);
        q_bezier(0,0,0,90,200,0);
    }

translate([200,0,0])mirror([0,1,0])q_bezier(0,0,300,150,260,0);
}
//hull(){
    translate([0,0,21])cylinder(r1=3.6, r2=5.2, h=2.4);
    //cylinder(r=)
//}
/********CORPO********/
scale([1.3,1.3,1.3])difference() {
    union() {
        translate ([0,0,24]) sphere(4); //testa
        translate([0,0,18]) cylinder(r=4,h=6); //testa
    }
    translate([2.5,-1.5,18]) cube([4,3,4]);
    //translate([3,-4,21]) cube([3,8,1.5]);
    translate([5.5,0,22]) mirror([1,0,1]) cylinder(r=1.5, h=3 );
}
translate ([0,0,19]) rotate([90,90,0]) cylinder(r=1.2,h=8); //braccio

translate([-0.3,-0.3,3])hull(){
translate([3.9,-1,5]) rotate([0,-10,45]) cube([7,7,7]);
translate([-0.1,0,8]) rotate([80,0,130]) scale([0.3,0.3,0.3])
    difference(){
        sphere(40);
        translate([0,0,-8]) cube(80,center=true);
    }
}