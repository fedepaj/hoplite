$fn=200;
rotate_extrude()translate([150,0,0])mirror([1,-1,0])difference(){
    union(){
        translate([0,-150,0])square([400,150]);
        q_bezier(0,0,0,90,200,0);
    }

translate([200,0,0])mirror([0,1,0])q_bezier(0,0,80,80,200,0);
}
module q_bezier(x0,y0,x1,y1,x2,y2){
    hull()for(t=[0:0.001:1]){
            x=pow((1-t),2)*x0+2*t*(1-t)*x1+pow(t,2)*x2;
            y=pow((1-t),2)*y0+2*t*(1-t)*y1+pow(t,2)*y2;
            translate([x,0,0])square([2,y]);
    }
}